﻿using System;
using UnityEngine;


[Serializable]
public class PlayerManager
{
    public int playernum;
    public GameObject instance;
    public Transform SpawnPoint;

    private PlayerMovement movement;
    private Gun gun;
    private CameraController camera;

    public void Setup()
    {
        movement = instance.GetComponentInChildren<PlayerMovement>();
        gun = instance.GetComponentInChildren<Gun>();
        camera = instance.GetComponentInChildren<CameraController>();

        movement.player_num = playernum;
        gun.player_num = playernum;
        camera.player_num = playernum;

        MeshRenderer[] renderers = instance.GetComponentsInChildren<MeshRenderer>();

    }
}
