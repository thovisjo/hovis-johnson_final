﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadSize : MonoBehaviour
{
    Vector3 sizeadded = new Vector3(.3f, .7f, .3f);
    public int headpoints;
    int player_num;
    
    // Start is called before the first frame update
    void Start()
    {
        player_num = gameObject.GetComponentInParent<Gun>().player_num;
        gameObject.layer = LayerMask.NameToLayer("HeadA" + player_num);
    }

    // Update is called once per frame
    void Update()
    {
        //IncreaseSize();
    }

    public void IncreaseSize()
    {
        gameObject.transform.localScale *= (1f + sizeadded[1]);
        GetComponentInParent<SphereCollider>().radius *= (1f + sizeadded[1]);
    }

    public void DecreaseSize()
    {
        if (gameObject.transform.localScale[1] > 1f)
        {
            gameObject.transform.localScale *= sizeadded[2];
            GetComponentInParent<SphereCollider>().radius *= sizeadded[2];
        }
    }
}
