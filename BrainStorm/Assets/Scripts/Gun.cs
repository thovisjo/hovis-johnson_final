﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [RangeAttribute(0.5f,1.5f)]
    public float fireRate = 1;

    [Range(1,100)]
    public int damage = 1;

    public int player_num;

    public float timer;

    public Transform firepoint;

    public int camnum;

    private GameObject camerarig; 

    private Camera fireray;

    LineRenderer gunLine;

    private float linetimer = 0f;

    private float linereset = .3f;

    public AudioSource lasersound;
    public AudioClip shot;

    private int layermask;

    LayerMask mask;

    // Update is called once per frame
    private void Start()
    {

        /*camerarig = this.transform.Find("PlayerCam").gameObject;

        //camerarig = GameObject.FindGameObjectWithTag("Cam" + player_num);
        fireray = GetComponentInChildren<Camera>(); camerarig.GetComponent<Camera>();
        camnum = gameObject.GetComponentInChildren<CameraController>().player_num;
        */
        if (player_num == 1)
        {
            gameObject.layer = 17;
            mask = LayerMask.GetMask("Player4", "Player2", "Player3", "Default");
            //mask = 17;
        }
        if (player_num == 2)
        {
            gameObject.layer = 18;
            mask = LayerMask.GetMask("Player4", "Player1", "Player3", "Default");
            //mask = 18;
        }
        if (player_num == 3)
        {
            gameObject.layer = 19;
            mask = LayerMask.GetMask("Player4", "Player2", "Player1", "Default");
            //mask = 19;
        }
        if (player_num == 4)
        {
            gameObject.layer = 20;
            mask = LayerMask.GetMask("Player1", "Player2", "Player3", "Default");
            //mask = 20;
        }
        //mask = ~mask;
        gunLine = GetComponentInChildren<LineRenderer>();
    }
    void Update()
    {
        if (camerarig == null)
        {
            camerarig = GameObject.FindGameObjectWithTag("Cam" + player_num);
        }

        timer += Time.deltaTime;
        linetimer += Time.deltaTime;

        if (timer >= fireRate)
        {
            if (Input.GetButton("Fire A " + player_num.ToString()))
            {
                timer = 0f;
                FireGun();

            }
        }
        if(linetimer > linereset)
        {
            gunLine.enabled = false;
            linetimer = 0;
        }
    }

    void FireGun()
    {
        lasersound.clip = shot;
        lasersound.pitch = Random.Range(.9f, 1.2f);
        Mathf.Clamp(lasersound.pitch, .8f, 1.2f);
        lasersound.volume = Random.Range(.8f, 1f);
        Mathf.Clamp(lasersound.volume, .8f, 1f);
        lasersound.Play();
        fireray = camerarig.GetComponent<Camera>();
        Ray ray = fireray.ViewportPointToRay(Vector3.one * 0.5f);
        RaycastHit hitinfo;
        Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 2f);
        

        if (Physics.Raycast(ray, out hitinfo, 100, mask))
        {
            ShotRender(hitinfo);
            var health = hitinfo.collider.GetComponent<Health>();
            if (health != null)
            {
                health.TakeDamage(damage);
            }
           
        }
        

    }

    void ShotRender(RaycastHit rayinfo)
    {
        gunLine.enabled = true;
        gunLine.SetPosition(0, gunLine.gameObject.transform.position);
        gunLine.SetPosition(1, rayinfo.point);
    }
}
