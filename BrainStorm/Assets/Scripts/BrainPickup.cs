﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainPickup : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0,1,0);
    }

    private void OnTriggerEnter(Collider other)
    {
        HeadSize headsize = other.GetComponentInChildren<HeadSize>();
        if(headsize != null)
        {
            headsize.IncreaseSize();
            Destroy(gameObject);
        }
    }
}
