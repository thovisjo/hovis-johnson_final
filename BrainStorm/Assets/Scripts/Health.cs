﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    // Start is called before the first frame update
    int startingHealth = 100;
    public int currentHealth;
    public GameObject[] spawnpoints;
    public Vector3 brainposition;
    public GameObject brainprefab;
    float braintimer;
    public float wait;

    private void Start()
    {
        spawnpoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
        braintimer = 0;
    }

    private void OnEnable()
    {
        currentHealth = startingHealth;
    }
    // Update is called once per frame
    void Update()
    {
        braintimer += 1;
    }

    public void TakeDamage(int damageAmount)
    {
        currentHealth -= damageAmount;

        if (currentHealth <= 0)
        {
            brainposition = gameObject.transform.position;
            Respawn();
            
        }
    }

    void Die()
    {
        //gameObject.SetActive(false);
        Respawn();
        
    }

    void Respawn()
    {
        gameObject.GetComponent<PlayerMovement>().enabled = false;
        int randomLoc = Random.Range(0, 4);
        gameObject.transform.position = spawnpoints[randomLoc].transform.position;
        gameObject.GetComponentInChildren<HeadSize>().DecreaseSize();
        StartCoroutine("spawnBrain");
        gameObject.GetComponent<PlayerMovement>().enabled = true;
        currentHealth = startingHealth;
        //Instantiate(brainprefab, new Vector3(brainposition[0], 1f, brainposition[2]), Quaternion.identity);
    }

    IEnumerator spawnBrain()
    {
        yield return new WaitForSeconds(.05f);
        Instantiate(brainprefab, new Vector3(brainposition[0], brainposition[1] + 1f, brainposition[2]), Quaternion.identity);

    }
}
