﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Vector2 mouseDirection;

    private Transform myBody;

    public int player_num;
    public Camera playerview;

    private void Start()
    {
        myBody = this.transform.parent.transform;

        Cursor.lockState = CursorLockMode.Locked;

        playerview = GetComponent<Camera>();

        //playerview.rect.width = .5;
        //playerview.rect.height = .5;

        if (player_num == 1)
        {
            Rect view1 = new Rect(0, 0.5f, .5f, .5f);
            playerview.rect = view1;
            gameObject.tag = "Cam1";
        }
        if(player_num == 2)
        {
            Rect view2 = new Rect(.5f, .5f, .5f, .5f);
            playerview.rect = view2;
            gameObject.tag = "Cam2";
        }
        if(player_num == 3)
        {
            Rect view3 = new Rect(0, 0, 0.5f, 0.5f);
            playerview.rect = view3;
            gameObject.tag = "Cam3";
        }
        if(player_num == 4)
        {
            Rect view4 = new Rect(0.5f, 0, .5f, 0.5f);
            playerview.rect = view4;
            gameObject.tag = "Cam4";
        }
        playerview.cullingMask = ~(1 << LayerMask.NameToLayer("HeadA" + player_num));
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.Joystick1Button7))
        {
            Cursor.lockState = CursorLockMode.None;
        }


        Vector2 mouseDelta = new Vector2(Input.GetAxisRaw("Joy X " + player_num.ToString()) * 10f, Input.GetAxisRaw("Joy Y " + player_num.ToString()) * 10f);

        
        mouseDirection += mouseDelta;

        mouseDirection.y = Mathf.Clamp(mouseDirection.y, -45, 45);

        this.transform.localRotation = Quaternion.AngleAxis(-mouseDirection.y, Vector3.right);

        myBody.localRotation = Quaternion.AngleAxis(mouseDirection.x, Vector3.up);

    }

}
