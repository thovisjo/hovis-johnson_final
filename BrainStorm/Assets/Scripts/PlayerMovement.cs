﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public int player_num;
    private CharacterController characterController;
    private Animator animator;
    public float movespeed = 100;
    public float turnspeed = 5;
    //Rigidbody rigidbody;

    void Awake()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponentInChildren<Animator>();
        //gameObject.layer = LayerMask.NameToLayer("Player" + player_num);
        //rigidbody = gameObject.GetComponent<Rigidbody>();
    }
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal " + player_num.ToString());
        float vertical = Input.GetAxis("Vertical " + player_num.ToString());
        Vector3 movement = new Vector3(horizontal, 0, vertical);

        animator.SetFloat("Speed", vertical);

        transform.Rotate(Vector3.up, horizontal * turnspeed * Time.deltaTime);

        if(vertical != 0)
        {
            characterController.SimpleMove(transform.forward * movespeed * vertical);
            //rigidbody.AddForce(movement*movespeed);

        }
        if(horizontal != 0)
        {
            characterController.SimpleMove(transform.right * movespeed * horizontal);
        }
    }
}
