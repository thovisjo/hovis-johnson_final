﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public float timeLeft = 30f;
    public Text timertext;
    public float Delay;

    public GameObject Player;
    public PlayerManager[] players;
    public string[] controllers;


    // Start is called before the first frame update

    void Start()
    {
        controllers = Input.GetJoystickNames();
        for(int controller = 0; controller < controllers.Length; controller++)
        {
            print(controllers[controller]);
        }
        SpawnAllPlayers();
    }

    // Update is called once per frame
    void Update()
    {
        timeLeft -= Time.deltaTime;

        timertext.text = "Timer: " + timeLeft.ToString();
        
        if(timeLeft <= 0)
        {
            GameOver();
        }
    }

    void SpawnAllPlayers()
    {
        for(int i = 0; i < players.Length; i++)
        {
            players[i].instance = Instantiate(Player, players[i].SpawnPoint.position, players[i].SpawnPoint.rotation) as GameObject;
            players[i].playernum = i + 1;
            players[i].Setup();
        }
    }

    void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
